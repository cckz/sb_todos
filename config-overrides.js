/* eslint-disable */

const {
  override,
  fixBabelImports,
  addLessLoader,
  addBundleVisualizer,
  addWebpackPlugin
} = require('customize-cra');
const { TypedCssModulesPlugin } = require('typed-css-modules-webpack-plugin');

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true
  }),
  addLessLoader({
    javascriptEnabled: true
  }),
  addWebpackPlugin(
    new TypedCssModulesPlugin({
      globPattern: 'src/**/*.css',
      camelCase: 'dashesOnly'
    })
  ),
  addBundleVisualizer({}, true)
);
