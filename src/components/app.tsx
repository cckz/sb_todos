import React, { useEffect } from "react";

import { loadTodos } from "./redux/actions";
import { useDispatch } from "react-redux";
import Content from "./content";
import Header from "./header";

const App: React.FC<{}> = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadTodos());
  }, [dispatch]);

  return (
    <div className="App">
      <Header />
      <Content />
    </div>
  );
};

export default App;
