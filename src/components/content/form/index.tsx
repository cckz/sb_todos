import { Button, Form, Icon, Input } from "antd";
import { FormComponentProps } from "antd/es/form";
import { addTodo } from "../../redux/actions";
import { useDispatch } from "react-redux";
import React, { FormEvent } from "react";
import css from "./index.module.css";
import faker from "faker";

faker.locale = "ru";

const FormTodo: React.FC<FormComponentProps> = ({ form }) => {
  const dispatch = useDispatch();

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    form.validateFields((err, todo) => {
      if (!err) {
        const newTodo = {
          ...todo,
          id: faker.random.uuid(),
          closed: false,
          name: faker.name.findName()
        };
        dispatch(addTodo(newTodo));
        form.resetFields();
      }
    });
  };

  return (
    <Form className={css.form} onSubmit={handleSubmit}>
      <Form.Item className={css.formItem}>
        {form.getFieldDecorator("title", {
          rules: [{ required: true, message: "Please todo title!" }]
        })(<Input placeholder="Type..." prefix={<Icon type="user" />} />)}
      </Form.Item>
      <Form.Item className={css.formItem}>
        <Button htmlType="submit" type="primary">
          Add Todo
        </Button>
      </Form.Item>
    </Form>
  );
};

export default Form.create({ name: "form_todo" })(FormTodo);
