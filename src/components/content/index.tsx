import { selectPreviewState } from "../redux/selectors";
import { useSelector } from "react-redux";
import FormTodo from "./form";
import ListTodos from "./list-todos";
import PreviewTodo from "./preview-todo";
import React from "react";
import css from "./index.module.css";

const Content: React.FC<{}> = () => {
  const { open, id } = useSelector(selectPreviewState);
  return (
    <main className={css.content}>
      <FormTodo />
      <div className={css.grid}>
        <ListTodos active={id} />
        {open && <PreviewTodo id={id} />}
      </div>
    </main>
  );
};

export default Content;
