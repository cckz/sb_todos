import { Button, Divider, List } from "antd";
import { Todo } from "../../types";
import { changeClosedState, openPreview } from "../../redux/actions";
import { selectAllTodos, selectLoadingState } from "../../redux/selectors";
import { useDispatch, useSelector } from "react-redux";
import React from "react";
import css from "./index.module.css";

interface ListTodosProps {
  active: string;
}
const ListTodos: React.FC<ListTodosProps> = ({ active }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(selectLoadingState);
  const todos: Todo[] = useSelector(selectAllTodos);

  return (
    <List
      bordered
      className={css.listTodo}
      dataSource={todos}
      loading={isLoading}
      pagination={{
        size: "small",
        pageSize: 15,
        hideOnSinglePage: true
      }}
      renderItem={(todo, index) => (
        <List.Item
          actions={[
            <Button icon="gitlab" key="1" />,
            <Button icon="github" key="2" />,
            <Button
              icon="double-right"
              key="3"
              onClick={() => {
                dispatch(openPreview(todo.id));
              }}
            />
          ]}
          className={active === todo.id ? css.active : undefined}
        >
          <div
            className={css.todoTitleWrapper}
            onClick={() => dispatch(changeClosedState(todo.id))}
            role="button"
          >
            <div className={css.title}>
              #{index + 1}. {todo.title}
            </div>
            <div className={css.name}>{todo.name}</div>
            {todo.closed && <Divider className={css.divider} />}
          </div>
        </List.Item>
      )}
      size="small"
    />
  );
};

export default ListTodos;
