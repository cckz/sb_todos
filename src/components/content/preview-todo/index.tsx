import { Card, Typography } from "antd";
import { StoreState } from "../../../store/rootReducer";
import { useSelector } from "react-redux";
import React from "react";
import css from "./index.module.css";

const { Text } = Typography;

interface PreviewProps {
  id: string;
}
const PreviewTodo: React.FC<PreviewProps> = ({ id }) => {
  const todo = useSelector((state: StoreState) => state.todos.list[id]);
  return (
    <Card className={css.card}>
      <Text>#{todo.id}</Text>
      <br />
      <Text>{todo.title}</Text>
      <br />
      <Text>{todo.name}</Text>
      <br />
      <Text>{todo.closed ? "Сделано" : "Не сделано"}</Text>
    </Card>
  );
};

export default PreviewTodo;
