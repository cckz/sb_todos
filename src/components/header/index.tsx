import React from "react";
import css from "./index.module.css";

const Header: React.FC<{}> = () => (
  <header className={css.header}>
    <div className={css.item} />
    <div className={css.item} />
  </header>
);

export default Header;
