import { Todo, Todos } from "../types";

export const ActionTypes = {
  ADD: "TODO/ADD" as const,
  CHANGE_CLOSED_STATE: "TODO/CHANGE_CLOSED_STATE" as const,
  CHANGE_PREVIEW_STATE: "TODO/CHANGE_PREVIEW_STATE" as const,
  LOAD_TODO: "TODO/LOAD" as const,
  LOAD_TODO_SUCCESS: "TODO/LOAD_SUCCESS" as const,
  LOAD_TODO_FAILURE: "TODO/LOAD_FAILURE" as const
};

export const addTodo = (todo: Todo) => ({
  type: ActionTypes.ADD,
  todo
});

export const changeClosedState = (id: string) => ({
  type: ActionTypes.CHANGE_CLOSED_STATE,
  id
});

export const openPreview = (id: string) => ({
  type: ActionTypes.CHANGE_PREVIEW_STATE,
  id
});

export const loadTodos = () => ({
  type: ActionTypes.LOAD_TODO
});

export const loadTodosSuccess = (todos: Todos) => ({
  type: ActionTypes.LOAD_TODO_SUCCESS,
  todos
});

export const loadTodosFailure = (error: string) => ({
  type: ActionTypes.LOAD_TODO_FAILURE,
  error
});
