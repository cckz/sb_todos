import { ActionTypes } from "./actions";
import { GenericAction } from "../../store/actions";
import { Todo } from "../types";
import { combineReducers } from "redux";

type TodoState = { [key: string]: Todo };
function list(state: TodoState = {}, action: GenericAction) {
  switch (action.type) {
    case ActionTypes.ADD: {
      const { todo } = action;
      return { ...state, [todo.id]: action.todo };
    }

    case ActionTypes.CHANGE_CLOSED_STATE: {
      const todo = state[action.id];
      return { ...state, [action.id]: { ...todo, closed: !todo.closed } };
    }

    case ActionTypes.LOAD_TODO_SUCCESS:
      return { ...action.todos };

    default:
      return state;
  }
}

export type PreviewState = { open: boolean; id: string };
const initState: PreviewState = {
  open: false,
  id: ""
};
function preview(state = initState, action: GenericAction) {
  switch (action.type) {
    case ActionTypes.CHANGE_PREVIEW_STATE: {
      const { id } = action;
      return id === state.id ? initState : { open: true, id: action.id };
    }

    default:
      return state;
  }
}

function isLoading(state: boolean = false, action: GenericAction) {
  switch (action.type) {
    case ActionTypes.LOAD_TODO:
      return true;
    case ActionTypes.LOAD_TODO_SUCCESS:
    case ActionTypes.LOAD_TODO_FAILURE:
      return false;

    default:
      return state;
  }
}

function error(state: string = "", action: GenericAction) {
  if (action.type === ActionTypes.LOAD_TODO_FAILURE) {
    return action.error;
  }
  return state;
}

export default combineReducers({ list, preview, isLoading, error });
