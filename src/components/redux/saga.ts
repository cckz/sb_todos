import * as schema from "./normalize";
import {
  ActionTypes,
  loadTodos,
  loadTodosFailure,
  loadTodosSuccess
} from "./actions";
import { all, delay, put, takeEvery } from "redux-saga/effects";
import { schema as n, normalize } from "normalizr";
import fixture from "./todos-fixture";

export default function* saga() {
  yield all([
    takeEvery(ActionTypes.LOAD_TODO, function*(
      action: ReturnType<typeof loadTodos>
    ) {
      try {
        yield delay(Math.round(200 + 500 * Math.random()));
        const { entities } = normalize(fixture, new n.Array(schema.todo));
        yield put(loadTodosSuccess(entities.todos));
      } catch (e) {
        yield put(loadTodosFailure(e));
      }
    })
  ]);
}
