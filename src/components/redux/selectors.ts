import { PreviewState } from "./reducer";
import { StoreState } from "../../store/rootReducer";
import { Todo } from "../types";

export const selectAllTodos = (state: StoreState): Todo[] =>
  Object.values(state.todos.list);

export const selectPreviewState = (state: StoreState): PreviewState =>
  state.todos.preview;

export const selectLoadingState = (state: StoreState): boolean =>
  state.todos.isLoading;
