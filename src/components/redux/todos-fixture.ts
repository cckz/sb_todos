import { Todo } from "../types";
import faker from "faker";

faker.locale = "ru";

const fixture: Todo[] = [];
for (let i = 0; i < 5; i++) {
  fixture.push({
    id: faker.random.uuid(),
    title: faker.lorem.text(),
    closed: faker.random.boolean(),
    name: faker.name.findName()
  });
}

export default fixture;
