export interface Todo {
  id: string;
  closed: boolean;
  title: string;
  name: string;
}

export interface Todos {
  [key: string]: Todo;
}
