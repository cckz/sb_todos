import { SubType } from "../lib/types";
import { actions as todoAcyion } from "../components/redux";

type ALL_ACTIONS = SubType<typeof todoAcyion, Function>;

// eslint-disable-next-line import/prefer-default-export
export type GenericAction = ReturnType<ALL_ACTIONS[keyof ALL_ACTIONS]>;
