import { combineReducers } from "redux";
import { todos } from "../components/redux";

const rootReducer = combineReducers({ todos });

export type StoreState = ReturnType<typeof rootReducer>;

export default rootReducer;
