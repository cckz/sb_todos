import { all, fork } from "redux-saga/effects";
import { saga as todoSaga } from "../components/redux";

export default function*() {
  return yield all([fork(todoSaga)]);
}
